from PIL import Image
import json, sys, os

if len(sys.argv) <3:
    print "Usage: Three Commandline arguments foldername, pathto json, path to mov"

jsondat = sys.argv[2]

#Get dictionary of frames
frame_dict = {}
with open(jsondat) as json_data:
    d = json.load(json_data)
    for item in d:
        label = d[item]["label"]
        for frame in d[item]["boxes"]:
            if not frame in frame_dict:
                frame_dict[frame] = []
            temp_list = [d[item]["boxes"][frame]["xtl"], d[item]["boxes"][frame]["ytl"], d[item]["boxes"][frame]["xbr"], d[item]["boxes"][frame]["ybr"], label]
            frame_dict[frame].append(temp_list)

#Iterate over frames
for frame in frame_dict:
    #Make directory for the frame
    foldername = sys.argv[1]+'/'+str(frame)
    if not os.path.exists(foldername):
        os.makedirs(foldername)
    #Open image file
    filename = sys.argv[1]+'/frame_'+frame+'.jpg'
    im = Image.open(filename)
    #Save a copy in the new folder
    im.save(foldername+'/original.jpg')
    #For each frame iterate over objects
    labelcounter = {}
    for obj in range(len(frame_dict[frame])):
        objlabel = frame_dict[frame][obj][-1]
        if objlabel in labelcounter:
            labelcounter[objlabel] += 1
        else:
            labelcounter[objlabel] = 1
        box = (frame_dict[frame][obj][0], frame_dict[frame][obj][1],
               frame_dict[frame][obj][2], frame_dict[frame][obj][3])
        #Crop out the box
        cutout = im.crop(box)
        #Save the cutout in the new folder
        cutout.save(foldername+'/'+objlabel+str(labelcounter[objlabel])+'.jpg')
    #Remove the processed image
    os.remove(filename)
