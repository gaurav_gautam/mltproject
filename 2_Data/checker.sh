OUTDIR=$1
JSONFILE=$2
MOVFILE=$3

#Check if image directory exists
if [ -d $OUTDIR ]; then	
	if [ "$(ls -A $OUTDIR)" ]; then
		rm -r $OUTDIR/*;			
	fi;
else						
	mkdir $OUTDIR;			
fi;							

#Check if the input files are present
if ! [ -f $JSONFILE ];								
then																
	echo "ERROR: You need $JSONFILE in the folder 2_Data";	
fi;	

if ! [ -f $MOVFILE ];									
then																
	echo "ERROR: You need $MOVFILE in the folder 2_Data";	
fi;
