#Steps to generate data

If you have videos in .dav format the convert them using:
```{sh}
ffmpeg -i filename.dav filename.mov
```
Keep the original filename for make to work

1. Make the checker.sh file executatble by running:

```{sh}
chmod a+x checker.sh
```

2. Get all the video files from ftp site into the `2_Data` folder.

3. Run make

```{sh}
make set{n}
```
where n=1,2,3 or 4. Look in the Makefile to see how long the videos in each set are.

4. In the output that you see there will be line like `skipped objects {'Bicycle:230', ...}`. This shows 
how many objects were skipped. If you want to increase(decrease) this number then you should decrease(increase)
the threshold in `line 171` of cutmaxpadboxes.py. You can also comment out the whole thing between the dashed 
lines to completely turn off the skipping feature. 

#Switching R and B channels for images

To do this run the switchRBchannels.py as follows

```{sh}
python switchRBchannels.py dataset2/Person dataset2/Car 
```
You can add as many folders as you like.

#Resizing images in filtered dataset
To resize the images in hand picked dataset to appropriate box size use:

```{sh}
python resizeimages.py folderpath/to/images classLabelOfImages path/to/json/file/from/which/to/get/boxsize
```
