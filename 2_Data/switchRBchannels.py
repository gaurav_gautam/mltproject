import sys, os
import cv2
from PIL import Image
import numpy as np

#Folder names as commandline arguments
folders = sys.argv[1:]

for folder in folders:
    #Get the filenames
    filenames = os.listdir(folder)
    
    for f in filenames:
        #Open the file
        im = cv2.imread(os.path.join(folder,f))
        im2 = im.copy()

        #Exchange the R and B channels
        im2[:,:,0] = im[:,:,2]
        im2[:,:,2] = im[:,:,0]
        
        cv2.imwrite(os.path.join(folder, f), im2)
