import json

frame_dict  = {}
with open('datasample1.json') as json_data:
    d = json.load(json_data)
    for item in d:
        label = d[item]["label"]
        for frame in d[item]["boxes"]:
            if not frame in frame_dict:      
                frame_dict[frame] = []
            if not (d[item]["boxes"][frame]['outside'] == 1 or d[item]["boxes"][frame]['occluded'] == 1):
                temp_list = [d[item]["boxes"][frame]["xtl"], d[item]["boxes"][frame]["ytl"], d[item]["boxes"][frame]["xbr"], d[item]["boxes"][frame]["ybr"], label]
                frame_dict[frame].append(temp_list)

#Find the max box sizes of various classes
obj_dict = {}
for frame in frame_dict:
    objs = frame_dict[frame]
    for props in objs:
        label = props[-1]
        if not (label in obj_dict):
            obj_dict[label] = [props[2]-props[0], props[3]-props[1]]
        else:
            currw = props[2]-props[0]
            currh = props[3]-props[1]
            if obj_dict[label][0] < currw:
                obj_dict[label][0] = currw 
       
            if obj_dict[label][1] < currh:
                obj_dict[label][1] = currh 

