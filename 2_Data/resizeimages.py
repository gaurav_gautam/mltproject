from getboxsizes import getmaxboxes, getminboxes, getavgboxes
import sys, os
from PIL import Image
import json

jsondat = sys.argv[3]
#Get dictionary of frames
frame_dict = {}
with open(jsondat) as json_data:
    d = json.load(json_data)
    for item in d:
        label = d[item]["label"]
        for frame in d[item]["boxes"]:
            if not frame in frame_dict:
                frame_dict[frame] = []
	    if not (d[item]["boxes"][frame]['outside'] == 1 or d[item]["boxes"][frame]['occluded'] == 1):
                temp_list = [d[item]["boxes"][frame]["xtl"], d[item]["boxes"][frame]["ytl"], d[item]["boxes"][frame]["xbr"], d[item]["boxes"][frame]["ybr"], label]
                frame_dict[frame].append(temp_list)

obj_dict = getavgboxes(frame_dict)

#Read folder name and label
folder = sys.argv[1]
label = sys.argv[2]

files = os.listdir(folder)
for f in files:
    im = Image.open(os.path.join(folder, f))
    im = im.resize(obj_dict[label])
    im.save(os.path.join(folder, f))

