#Find the max box sizes of various classes
def getmaxboxes(frame_dict):
    obj_dict = {}
    for frame in frame_dict:
        objs = frame_dict[frame]
        for props in objs:
            label = props[-1]
            if not (label in obj_dict):
                obj_dict[label] = [props[2]-props[0], props[3]-props[1]]
            else:
                currw = props[2]-props[0]
                currh = props[3]-props[1]
                if obj_dict[label][0] < currw:
                    obj_dict[label][0] = currw 
       
                if obj_dict[label][1] < currh:
                    obj_dict[label][1] = currh 
    return obj_dict

#Find the max box sizes of various classes
def getminboxes(frame_dict):
    obj_dict = {}
    for frame in frame_dict:
        objs = frame_dict[frame]
        for props in objs:
            label = props[-1]
            if not (label in obj_dict):
                obj_dict[label] = [props[2]-props[0], props[3]-props[1]]
            else:
                currw = props[2]-props[0]
                currh = props[3]-props[1]
                if obj_dict[label][0] > currw:
                    obj_dict[label][0] = currw 
       
                if obj_dict[label][1] > currh:
                    obj_dict[label][1] = currh
    return obj_dict

#Find the average box sizes of various classes
def getavgboxes(frame_dict):
    obj_dict = {}
    obj_counter = {}
    for frame in frame_dict:
        objs = frame_dict[frame]
        for props in objs:
            label = props[-1]
            if not (label in obj_dict):
                obj_dict[label] = [props[2]-props[0], props[3]-props[1]]
                obj_counter[label] = 1
            else:
                currw = props[2]-props[0]
                currh = props[3]-props[1]
                obj_counter[label] += 1
                obj_dict[label][0] += currw
                obj_dict[label][1] += currh 
    
    for obj in obj_dict:
        obj_dict[obj][0] = obj_dict[obj][0]/obj_counter[obj]
        obj_dict[obj][1] = obj_dict[obj][1]/obj_counter[obj]
    return obj_dict

