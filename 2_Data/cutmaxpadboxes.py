#Look for very long lines like this one-----------------------------------------------------------------------------------------------------
#These are the lines that you can comment/uncomment
#-------------------------------------------------------------------------------------------------------------------------------------------
from getboxsizes import getavgboxes, getmaxboxes, getminboxes
from PIL import Image
import json, sys, os
import numpy as np
import cv2

def adjustbox(boxtup, maxsize):
    adjustedbox = list(boxtup)
    width = boxtup[2]-boxtup[0]
    height = boxtup[3]-boxtup[1]
    if width < maxsize[0]:
        rem = maxsize[0]-width 
        adjustedbox[0] -= int(np.floor(rem*0.5))
        adjustedbox[2] += int(np.ceil(rem*0.5))
    if height < maxsize[1]:
        rem = maxsize[1]-height
        adjustedbox[1] -= int(np.floor(rem*0.5))
        adjustedbox[3] += int(np.ceil(rem*0.5))
    
    return tuple(adjustedbox)
        
#main program starts
if len(sys.argv) <4:
    print "Usage: Three Commandline arguments foldername, pathto json, path to mov"

outfolder = sys.argv[1]
jsondat = sys.argv[2]

#Get dictionary of frames
frame_dict = {}
with open(jsondat) as json_data:
    d = json.load(json_data)
    for item in d:
        label = d[item]["label"]
        for frame in d[item]["boxes"]:
            if not frame in frame_dict:
                frame_dict[frame] = []
	    if not (d[item]["boxes"][frame]['outside'] == 1 or d[item]["boxes"][frame]['occluded'] == 1):
                temp_list = [d[item]["boxes"][frame]["xtl"], d[item]["boxes"][frame]["ytl"], d[item]["boxes"][frame]["xbr"], d[item]["boxes"][frame]["ybr"], label]
                frame_dict[frame].append(temp_list)

#UNCOMMENT TO CHOOSE WHAT THE SIZE OF SUBIMAGES WILL BE AFTER RESIZING--------------------------------------------------------------------
obj_dict = getavgboxes(frame_dict)
#obj_dict = getmaxboxes(frame_dict)
#obj_dict = getminboxes(frame_dict)
print obj_dict
#-----------------------------------------------------------------------------------------------------------------------------------------

#Make directories to store subimages of different classes
for label in obj_dict:
    if not os.path.exists(outfolder+'/'+label):
        os.mkdir(outfolder+'/'+label)

#Iterate over frames
cap = cv2.VideoCapture(sys.argv[3])
frame_nums = [int(i) for i in frame_dict.keys()]
lastbox = {}
skippedcounter = {}
for i in range(max(frame_nums)):
    labelcounter = {}
    #Skip video frames that are not annotated
    if str(i) in frame_dict:
        frame = str(i)
    else:
        print "frame", i, "not in frame_dict"
        ret, im = cap.read()
        continue

    #Get the frame from video
    ret, im = cap.read()
    #Convert numpy array to image instance
    im = Image.fromarray(im)

    #For each frame iterate over objects
    print "Processing frame_:", frame
    for obj in range(len(frame_dict[frame])):
        box = (frame_dict[frame][obj][0], frame_dict[frame][obj][1],
               frame_dict[frame][obj][2], frame_dict[frame][obj][3])

        objlabel = frame_dict[frame][obj][-1]
        if objlabel in labelcounter:
            labelcounter[objlabel] += 1
        else:
            labelcounter[objlabel] = 1
        

#CHECK IF THE FRAME IS TOO CLOSE TO THE LAST FRAME--------------------------------------------------------------------------------------
        #Add a box if none exist from earlier
        uniquelabel = objlabel + str(labelcounter[objlabel])
        print "Processing ", uniquelabel
        if uniquelabel not in lastbox:
            lastbox[uniquelabel] = box
        else:
            #Get all boxes in the last frame that are of this class
            labs = lastbox.keys() #All keys in last stored boxes dictionary
            oldboxes = []
            for label in labs:
                if objlabel in label:   #So if currently objlabel is Car then this is true fro Car1, Car2 etc in the last box
                    print "Getting box for: ", label
                    oldboxes.append(lastbox[label]) #For a car in current frame we get all Car boxes from last frame
            
            #Now calculate the minimum norm the current box has with the oldboxes
            norm = 100000000000000 #A very large number for initialization
            for oldbox in oldboxes:
                dist = (box[0]-oldbox[0], box[1]-oldbox[1])#, box[2]-oldbox[2], box[3]-oldbox[3])
                sumdist = np.sum(np.abs(dist))
                print sumdist
                if (sumdist < norm):
                    norm = sumdist
            print "Minimum norm:", norm 
            lastbox[uniquelabel] = box
            if (norm < 250):
                if objlabel in skippedcounter:
                    skippedcounter[objlabel] += 1
                else:
                    skippedcounter[objlabel] = 1
                #lastbox[uniquelabel] = box
                print "Skipped\n"
                continue
#---------------------------------------------------------------------------------------------------------------------------------------

#TURN ON/OFF THE RESIZING OF THE SUBIMAGES----------------------------------------------------------------------------------------------        
        #Resize the image
        #cutout = im.crop(box).resize(obj_dict[objlabel])
        
        #Don't resize the image
        cutout = im.crop(box)

        #Add paddings to get same size box for all instances. The size is avg, max or min as chosen earlier.
        #box = adjustbox(box, obj_dict[objlabel])
        #cutout = im.crop(box)
#----------------------------------------------------------------------------------------------------------------------------------------
        
        #Save the cutout in the new folder
        cutout.save(outfolder+'/'+objlabel+'/'+frame+'_'+str(labelcounter[objlabel])+'.jpg')

#Print number of skipped images
print "Skipped objects:", skippedcounter
