# Self attempt at background subtraction, simply subtract the mode from each frame
# Tunable : 4 and 50
# Resizing the frame

import numpy as np
import cv2
import skimage
import skvideo.io
import skimage.io as io
import pickle
from scipy import stats

def readVideo(path):
# Takes in path for video file and returns the nparray of all frames, as well as the avg Background
	cap = cv2.VideoCapture(path)
	ret, frame = cap.read()
	bgList = []
	while(ret):
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		grayFrame = cv2.resize(grayFrame ,None, fx=0.5, fy=0.5)	# Resize the frame, trying right now
		grayFrame = grayFrame.astype(np.uint8)
		bgList.append(grayFrame)
		ret, frame = cap.read()
	bgArr = np.asarray(bgList)		# Convert list to array
	avgBg = (np.sum(bgArr, axis = 0)/len(bgList)).astype(np.uint8)		# Find average background
	skimage.io.imsave('./Output/bgs1/avgBg.jpg', avgBg)
	cap.release()
	return bgArr, avgBg

def saveVideo(frameArr, path):
# Takes in array of frames and path for storing the frames of required video
	i = 0
	for x in frameArr:
		if(i>300 and i<500):
			skimage.io.imsave(path + str(i) + '.jpg', x)
		i += 1

if __name__ == "__main__":
	bgArr, Bg = readVideo('./Input/datasample1.mov')	# This Bg is average background
	Bg = stats.mode((bgArr/4)*4)[0][0]			
	# This Bg is mode background, 4 can be tuned, doing this speeds up mode computation
	skimage.io.imsave('./Output/bgs1/modeBg.jpg', Bg)

	bgArr = np.absolute(bgArr - Bg)	
	# saveVideo(np.absolute(bgArr - avgBg), './Output/bgs1/absDiff/')
	binArr = ((bgArr > 50)*255).astype(np.uint8)		# Binary array of thresholded images
	saveVideo(binArr, './Output/bgs1/threshDiff/')


# for x in binArr:
# 	# Blur the image first to get some noise out
# 	x = cv2.GaussianBlur(x, (5, 5), 3)
# 	# Opening is just another name of erosion followed by dilation. It is useful in removing noise
# 	kernel = np.ones((5,5),np.uint8)
# 	x = cv2.morphologyEx(x, cv2.MORPH_OPEN, kernel, iterations = 10)
# 	x = cv2.morphologyEx(x, cv2.MORPH_OPEN, kernel, iterations = 10)
# saveVideo(binArr, './Output/bgs1/temp/')

