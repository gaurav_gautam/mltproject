# It uses a method to model each background pixel by a mixture of K Gaussian distributions 
# (K = 3 to 5). The weights of the mixture represent the time proportions that those colours stay 
# in the scene.
# Parameters to be tuned:
# history
# nmixtures
# backgroundRatio
# learningRate

import numpy as np
import cv2
import skimage
import skvideo.io
import skimage.io as io
import pickle

cap = cv2.VideoCapture('./Input/datasample1.mov')

fgbg = cv2.BackgroundSubtractorMOG(history = 200, nmixtures=5, backgroundRatio = 0.01)
# Lower background-ratio parameter gives a more solid shape of a moving car.
# NoiseSigma parameter seems to be used to set the max. variance of each Gaussian component


framArr = []					# Nparray of all image frames

i = 0
ret, frame = cap.read()
while(ret):
    fgmask = fgbg.apply(frame, learningRate=0.001)
    # Default value of learningRate is 0, don't use that because it fixes the first frame in video.
    # Put a small value. A smaller value gives more solid results
    if i%10 == 0:
        print i
    if i>300:               # Store frames from above 300, less than that are shitty
        skimage.io.imsave('./Output/'+str(i)+'.jpg', fgmask)
        # framArr.append(frame)     
    i += 1
    ret, frame = cap.read()

cap.release()

# framArr = np.asarray(framArr)
# print framArr.shape
# print "Frames captured : ", framArr.shape[0]
# target = open('./Output/mog_framArr.p', 'wb')	# Dump the frame array in a pickle file
# pickle.dump(framArr,target)	
# target.close()	
