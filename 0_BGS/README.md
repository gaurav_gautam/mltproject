Module1 : *BGS*
===============

This module deals with trying multiple libraries for the task of backgorund subtraction.

Folder structure
----------------

Input : Contains the input videos to the BGS programs
Output : Contains the output videos
COde : Contains source code

This part deals with trying multiple libraries for the task of backgorund subtraction. Till now,
I have added 3 methods for the same
1. Mog1(Mixture of Gaussians) from OpenCV :
	> make mog
2. Mog2 from OpenCV :
	> make mog2
3. Self method where I find overall mode of all images as background and subtract it from each frame :
	> make bgs1

Overall mog2 and mog1 appear better till now, can be tuned.

Comment : I have used OpenCV 2.4 for this and my cv2.imshow() does not work. Therefore I first save all
frames for each video and then use memcoder to convert it to video. This is a stupid way but I could
not find any alternative. So please modify the code according to your version.


Notes
-----
1. Try using one frame at a time in the code rather than storing it in a big array because it is very
bad for memory. For example, in the mode finding method for background subtraction we store all frames.
But this won't be possible for larger videos. So try ignoring such methods and carry minimal information
forward. Therefore it will become a bit difficult to keep the code a lot modular
