# Self attempt at background subtraction, simply subtract the mode from each frame
# Tunable : 4 and 50
# Resizing the frame

import numpy as np
import cv2
import skimage
import skvideo.io
import skimage.io as io
import pickle
from scipy import stats

def readVideo(path):
# Takes in path for video file and returns the nparray of all frames, as well as the avg Background
	cap = cv2.VideoCapture(path)
	ret, frame = cap.read()
	avgBg = 1
	count = 0
	while(ret):
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		grayFrame = cv2.resize(grayFrame ,None, fx=0.5, fy=0.5)	# Resize the frame, trying right now
		if type(avgBg) == int:
			avgBg = grayFrame
			avgBg = avgBg.astype(float)
		else:
			avgBg += grayFrame
		count += 1
		ret, frame = cap.read()
	avgBg = (avgBg/count).astype(np.uint8)		# Find average background
	skimage.io.imsave('./Output/avgBg.jpg', avgBg)
	cap.release()
	return avgBg

def saveVideo(avgBg, path1, path2):
# Takes in array of frames and path for storing the frames of required video
	cap = cv2.VideoCapture(path1)
	ret, frame = cap.read()
	i = 0
	while(ret):
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		grayFrame = cv2.resize(grayFrame ,None, fx=0.5, fy=0.5).astype(float)	# Resize the frame, trying right now
		grayFrame = np.absolute(grayFrame - avgBg)
		binArr = ((grayFrame > 25)*255).astype(np.uint8)		# Binary array of thresholded images
		if(i>300):
			skimage.io.imsave(path2 + str(i) + '.jpg', binArr)
			# skimage.io.imsave(path2 + str(i) + '.jpg', grayFrame.astype(np.uint8))
		i += 1
		ret, frame = cap.read()

	cap.release()

if __name__ == "__main__":
	Bg = readVideo('./Input/datasample1.mov')	# This Bg is average background
	saveVideo(Bg, './Input/datasample1.mov', './Output/bgs1/threshDiff/')
