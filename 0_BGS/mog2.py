# Parameters to be tuned
# history
# varThreshold
# learningRate

import numpy as np
from PIL import Image
import cv2
import skimage
import skvideo.io
import skimage.io as io
import pickle
import sys

cap = cv2.VideoCapture(sys.argv[1])

fgbg = cv2.BackgroundSubtractorMOG2(history = 200, varThreshold = 50, bShadowDetection=False)
# varThreshold :	Threshold on the squared Mahalanobis distance between the pixel and the model to
# decide whether a pixel is well described by the background model
framArr = []					# Nparray of all image frames

i = 0
ret, frame = cap.read()
while(ret):
    fgmask = fgbg.apply(frame, learningRate=0.0007)
    # Default value of learningRate is 0, don't use that because it fixes the first frame in video.
    # Put a small value. A smaller value gives more solid results
    if i%10 == 0:
        print i
    if i>300:				# Store frames from above 500, less than that are shitty
        skimage.io.imsave('./Output/'+str(i)+'.jpg', fgmask)
        # framArr.append(frame)		
    i += 1
    ret, frame = cap.read()

cap.release()

# framArr = np.asarray(framArr)
# print framArr.shape
# print "Frames captured : ", framArr.shape[0]
# target = open('./Output/mog2_framArr.p', 'wb')	# Dump the frame array in a pickle file
# pickle.dump(framArr,target)	
# target.close()	
