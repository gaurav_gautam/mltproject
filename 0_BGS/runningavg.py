# Self attempt at background subtraction, simply subtract the mode from each frame
# Tunable : 4 and 50
# Resizing the frame

import numpy as np
import cv2
import skimage
import skvideo.io
import skimage.io as io
import pickle
from scipy import stats

def getAvg(path, frames, index):
    """
    Return the average frame at `index` by averaging over `frames` frames using the video `path`.
    """
    cap = cv2.VideoCapture(path)
    if (index-frames < 0):
        start = 0
        frames = index+1 #In case the index is 0 divide by 1 instead
    else:
        start = index-frames

    #Skip the frames
    for i in range(start):
        ret, frame = cap.read()

    #Start adding
    for i in range(start, index+1):
        ret, frame = cap.read()
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        grayFrame = cv2.resize(grayFrame ,None, fx=0.5, fy=0.5)
        if i == start:
            avgBg = grayFrame.astype(float)
        else:
            avgBg += grayFrame

    #Save the figure
    avgBg = (avgBg/frames).astype(np.uint8)
    skimage.io.imsave('./Output/runningavg/'+str(index)+'.jpg', avgBg)

def getFrames(path):
    cap = cv2.VideoCapture(path)
    length = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
    for i in range(length):
        getAvg(path, 200, i)

if __name__ == "__main__":
	getFrames('./Input/datasample1.mov')	# This Bg is average background
