Module2 : *Classify*
==================

This module deals with trying multiple methods for classifying input data from step 2

Folder structure
----------------

Input : Contains the input images from step 2

Overall Algorithm:
------------------
# Step 1 : Take images
# Step 2 : Convert to vectors
# Step 3 : Classify

gray:
-----
# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Normalizes it

Command : > make gray r=0/1

r value indicates whether we have to read the datafiles again(r=1) or can we simply use the already 
present pickle files(r=0)

hist1:
------
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Finds histogram and normalizes it

Command : > make hist1 r=0/1

hist2:
------
# This transform  :
# 1. Takes in image 
# 2. Converts to grayscale
# 3. Finds hierarchical histogram and normalizes it

Command : > make hist2 r=0/1

sift1:
------
# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Finds fixed number of sift features, appends them and normalizes it

Command : > make sift1 r=0/1

hog1:
-----
# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Finds histogram of gradients and normalizes it

Command : > make hog1 r=0/1