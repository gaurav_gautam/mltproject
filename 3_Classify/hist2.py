# This transform  :
# 1. Takes in image 
# 2. Converts to grayscale
# 3. Finds hierarchical histogram and normalizes it

# Tunable :
# Gaussian size
# Bin count
# Whether to individually normalize each vector, line 31

binCount = 64           # Number of bins in histogram
numLevels = 3           # Number of levels in hierarchy, hist1 is same as numLevels = 1
import cv2
import numpy as np

def transform(img):
    # This function takes in an img and converts it into a vector for classification

    # Convert to grayscale because colour not important
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Blur the img
    img = cv2.GaussianBlur(img, (3, 3), 0)      # Tunable, since small images
    imgArr =  np.empty(shape=(0,), dtype='int')   
    for i in range(0,numLevels):                # For level i
        for j in range(0, 2**i):
            for k in range(0, 2**i):
                mask = np.zeros(img.shape[:2], np.uint8)
                mask[ j*img.shape[0]/2**i:(j+1)*img.shape[0]/2**i, k*img.shape[1]/2**i: (k+1)*img.shape[1]/2**i] = 255
                hist_mask = cv2.calcHist([img],[0],mask,[binCount],[0,256])
                hist_mask = np.squeeze(hist_mask).astype('int')
                hist_mask = hist_mask / np.linalg.norm(hist_mask)
                imgArr = np.concatenate((imgArr,hist_mask))
    imgArr = imgArr / np.linalg.norm(imgArr)
    return imgArr