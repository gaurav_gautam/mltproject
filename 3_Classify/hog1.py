# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Finds histogram of gradients and normalizes it

# Tunable :
# Gaussian size
# Bin count
# Resizing the image
import cv2
import numpy as np
from skimage.feature import hog
def transform(image):
    # This function takes in an image and converts it into a vector for classification
# for filename in os.listdir('./'):
# 	image = io.imread(filename)
	# Convert to grayscale because colour not important
        if len(image.shape) == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	# Resize for now
	image = cv2.resize(image, (100, 100))
	# Blur the image
	image = cv2.GaussianBlur(image, (3, 3), 0)      # Tunable, since small images
	hist= hog(image, orientations=8, pixels_per_cell=(16,16), cells_per_block=(1, 1))
	hist = hist / np.linalg.norm(hist)     # Normalize the vector
	# print len(hist)
	return hist

