# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Finds histogram and normalizes it

# Tunable :
# Gaussian size
# Bin count
binCount = 64
import cv2
import numpy as np
def transform(image):
    # This function takes in an image and converts it into a vector for classification

    # Convert to grayscale because colour not important
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Blur the image
    image = cv2.GaussianBlur(image, (3, 3), 0)      # Tunable, since small images
    hist = cv2.calcHist([image],[0],None,[binCount],[0,256]) # Find histogram with 64 bins
    hist = np.squeeze(hist).astype('int')
    hist = hist / np.linalg.norm(hist)     # Normalize the vector
    return hist


