# This file is basically imported  by other files to build a classifier and finally runs a 
# cros-validation classifier on it

# Tunable
# label set
# n_clusters
# K for K nearest neighbors
# Distance metric for k nearest neighbors

import numpy as np
import os, sys, re, cv2, time
import skimage.io as io
import skimage.color as color
import pickle
from sklearn.neighbors import KNeighborsClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix
from sklearn.cluster import KMeans
from importlib import import_module
mod = import_module(sys.argv[1])    # Import the module as passed from commandline

# Global dictionary to store data
# dataDict['Bicycle'] stores the data for bicycles, and so on
dataDict = {}
# The labels I am considering for now
labels = ['Bicycle', 'Car', 'Motorcycle', 'Person']

def shuffle_in_unison(a, b):

    # Takes in two numpy arrays and shuffles them in unison, ie. together   
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)


def readFiles():
    # This function builds your dataDict by reading the actual input data files and converting them 
    # to vectors, and stores them in a pickle file. Therefore no need to call this function multiple times
    global dataDict
    print 'Starting to read files'
    for i in range(0,len(labels)):   # For each data set      
        dataList = []
        for filename in os.listdir('./Input/' + labels[i]):    # For each image of this label
            image = io.imread('./Input/' + labels[i] + '/' + filename)
            imgArr = mod.transform(image)
            if len(imgArr) == 0:
                continue
            dataList.append(imgArr)  # Append all image vectors to a list   
        dataDict[labels[i]] = np.array(dataList)
        print "\rReading files completed : " + str((i+1)*100.0/len(labels)) + "%",
    target = open('dataDict.p', 'wb')
    pickle.dump(dataDict,target)    
    target.close() 

n_clusters = 16  # Tunable    
def clusterAndClassify():
    # This function builds your dataDict by reading a pickle file, and then generates cluster centers
    # For training data, we are using the cluster centers
    # For test data, we are using the entire image training set

    global dataDict
    target = open('dataDict.p', 'r')
    dataDict = pickle.load(target)
    target.close()

    trainData = []
    trainLabel = []

    print 'Number of clusters for each class : ' + str(n_clusters)
    for label in dataDict:
        inputData =  list(dataDict[label])
        cls = KMeans(n_clusters=n_clusters, n_jobs=-1)
        cls.fit(inputData)
        i = 0
        for arr in cls.cluster_centers_:    # Save each average cluster center
            trainData += [arr]
            trainLabel += [label]
            img= np.reshape(arr, (-1,100))
            img = img.astype('uint8')
            io.imsave('./Output/' + label +'/' + str(i) + '.jpg', img)
            i = i+1
    shuffle_in_unison(trainData, trainLabel)
    print 'Train data created'

    # Take equal number of images from each dataset
    numImages = 99999
    for label in dataDict.keys():   # For each data set      
        numImages = min(numImages, dataDict[label].shape[0])

    testData = []
    testLabel = []  
    for label in dataDict:
        testData +=  list(dataDict[label])[0:numImages]  
        testLabel += [label]*numImages 
        # inputLabel += [label]*dataDict[label].shape[0] 
    # Shuffle the data for cross validation
    shuffle_in_unison(testData, testLabel)
    print 'Test data created'

    # Simply classify
    classify(trainData, trainLabel, testData, testLabel )       


def classify(trainData, trainLabel, testData, testLabel ):
    # This function simply takes in the data and classifies it
    K = 3    # Tunable
    print "Using K-Nearest Neighbors with K = " + str(K)
    knClassifier = KNeighborsClassifier(n_neighbors = K, weights='distance', n_jobs = -1)
    knClassifier.fit(trainData, trainLabel)
    print '\nClassifying accuracy: ', knClassifier.score(testData,testLabel)
    pred_label = knClassifier.predict(testData)
    cm = confusion_matrix(testLabel, pred_label)
    print 'Labels : ', dataDict.keys() 
    print 'Confusion matrix : '
    print cm

if __name__ == "__main__":
    if int(sys.argv[2]):
        readFiles()
    clusterAndClassify()
