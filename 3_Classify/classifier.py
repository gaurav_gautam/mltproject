# This file is basically imported  by other files to build a c lassifier and finally runs a 
# cros-validation classifier on it

# Tunable
# label set

import numpy as np
import os, sys, re, cv2, time
import skimage.io as io
import skimage.color as color
import pickle
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix
from importlib import import_module
mod = import_module(sys.argv[1])    # Import the module as passed from commandline


# Global dictionary to store data
# dataDict['Bicycle'] stores the data for bicycles, and so on
dataDict = {}

# The labels I am considering for now
labels = ['Bicycle', 'Car', 'Motorcycle', 'Person']

mod = import_module(sys.argv[1])

def shuffle_in_unison(a, b):
    # Takes in two numpy arrays and shuffles them in unison, ie. together
    
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)


def readFiles():
    # This function builds your dataDict by reading the actual input data files and converting them 
    # to vectors, and stores them in a pickle file. Therefore no need to call this function multiple times
    global dataDict
    print 'Starting to read files'
    for i in range(0,len(labels)):   # For each data set      
        dataList = []
        for filename in os.listdir('./Input/' + labels[i]):    # For each image of this label
            image = io.imread('./Input/' + labels[i] + '/' + filename)
            imgArr = mod.transform(image)
            if len(imgArr) == 0:    # No array returned
                continue
            dataList.append(imgArr)  # Append all image vectors to a list   
        dataDict[labels[i]] = np.array(dataList)
        print "\rReading Completed : " + str((i+1)*100.0/len(labels)) + "%",
    target = open('dataDict.p', 'wb')
    pickle.dump(dataDict,target)    
    target.close() 

    
def getData():
    # This function builds your dataDict by reading a pickle file

    global dataDict
    target = open('dataDict.p', 'r')
    dataDict = pickle.load(target)
    target.close()

    # Take equal number of images from each dataset
    numImages = 99999
    for label in dataDict.keys():   # For each data set      
        numImages = min(numImages, dataDict[label].shape[0])
    # print numImages

    inputData = []
    inputLabel = []
    for label in dataDict:
        inputData +=  list(dataDict[label])[0:numImages] 
        inputLabel += [label]*numImages 
            # inputLabel += [label]*dataDict[label].shape[0] 
    # Shuffle the data for cross validation
    shuffle_in_unison(inputData, inputLabel)
    return inputData, inputLabel


def crossValidate(inputData, inputLabel):
    # This function takes in inputData and inputLabel and crossvalidate different classifiers on it

    n = 5
    score = 0.0
    print 'Cross validation with ' + str(n) + ' passes' 
    for i in range(0,n):
        print "\nPass " + str(i+1)
        testData = inputData[ (i)*len(inputLabel)/5 : (i+1)*len(inputLabel)/5 ]
        testLabel = inputLabel[ (i)*len(inputLabel)/5 : (i+1)*len(inputLabel)/5 ]
        trainData = inputData[:(i)*len(inputLabel)/5] + inputData[(i+1)*len(inputLabel)/5:]
        trainLabel = inputLabel[:(i)*len(inputLabel)/5] + inputLabel[(i+1)*len(inputLabel)/5:]

        print "Using multiclass SVM"
        cls = OneVsRestClassifier(LinearSVC(random_state=0), n_jobs=-1)
        cls.fit(trainData, trainLabel)
        temp = cls.score(testData, testLabel)
        score += temp
        print "Classifying accuracy : " + str(temp) 
        predLabel = cls.predict(testData)
        cm = confusion_matrix(testLabel, predLabel)
        print 'Labels : ', dataDict.keys() 
        print 'Confusion matrix : '
        print cm        
    print "\nOverall Average classifying accuracy : " + str(score*1.0/n)

if __name__ == "__main__":
    # Read files if instructed from commandline
    if int(sys.argv[2]):
        readFiles()
    inputData, inputLabel = getData()
    crossValidate(inputData, inputLabel)