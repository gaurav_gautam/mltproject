# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Normalizes it

# Tunable :
# Gaussian size
# Resize the image
import cv2
import numpy as np
def transform(image):
	# This function takes in an image and converts it into a vector for classification

	# Convert to grayscale because colour not important
	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	# Resize the image, temporary
	image = cv2.resize(image, (100, 100))
	# Blur the image
	image = cv2.GaussianBlur(image, (3, 3), 0)      # Tunable, since small images
	image = image.flatten()
	return image
