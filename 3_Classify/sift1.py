# This transform  :
# 1. Takes in image, 
# 2. Converts to grayscale
# 3. Finds fixed number of sift features, appends them and normalizes it

# Tunable :
# Gaussian size
# Resize for now
# nfeatures for SIFT

import cv2
import numpy as np
import skimage.io as io
nfeatures = 50
sift = cv2.SIFT(nfeatures = nfeatures)	# SIFT
def transform(image):
	# This function takes in an image and converts it into a vector for classification

	# Convert to grayscale because colour not important
	image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	# Resize the image, temporary
	image = cv2.resize(image, (100, 100))
	# Blur the image
	image = cv2.GaussianBlur(image, (3, 3), 0)      # Tunable, since small images
	_,des = sift.detectAndCompute(image,None)
	# if des == None:
	# 	print image
	# 	io.imshow(image)
	# 	io.show()
	if not des.any() or len(des) < nfeatures:
		return []
	des = des[0:nfeatures]
	des = des.flatten()
	des = des / np.linalg.norm(des)     			# Normalize the vector
	return des
