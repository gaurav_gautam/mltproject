# This file is basically imported  by other files to build a c lassifier and finally runs a 
# cros-validation classifier on it

# Tunable
# label set

import numpy as np
import os, sys, re, cv2, time
import skimage
import skimage.io as io
import skimage.color as color
import pickle
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix
from importlib import import_module
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
mod = import_module(sys.argv[1])    # Import the module as passed from commandline


# Global dictionary to store data
# dataDict['Bicycle'] stores the data for bicycles, and so on
dataDict = {}

# The labels I am considering for now
labels = ['Person', 'Nonperson']

mod = import_module(sys.argv[1])
#cls = OneVsRestClassifier(LinearSVC(random_state=0), n_jobs=-1)
# cls = RandomForestClassifier(n_estimators=200, n_jobs=-1, max_depth=16)
cls = AdaBoostClassifier(DecisionTreeClassifier(max_depth=4), n_estimators = 120)


def shuffle_in_unison(a, b):
    # Takes in two numpy arrays and shuffles them in unison, ie. together
    
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)

def readFiles():
    # This function builds your dataDict by reading the actual input data files and converting them 
    # to vectors, and stores them in a pickle file. Therefore no need to call this function multiple times
    print 'Starting to read files'
    trainData = []
    trainLabel = []
    for i in range(0,len(labels)):   # For each data set      
        for filename in os.listdir('./Input/Train/' + labels[i]):    # For each image of this label
            image = io.imread('./Input/Train/' + labels[i] + '/' + filename)
            imgArr = mod.transform(image)
            if len(imgArr) == 0:    # No array returned
                continue
            trainData.append(imgArr)  # Append all image vectors to a list   
        trainLabel += [labels[i]]*len( os.listdir('./Input/Train/' + labels[i]) )

    print "Training data read completely :", len(trainData), len(trainLabel)
    target = open('./pickle/trainData_' + sys.argv[1] + '.p', 'wb')
    pickle.dump(trainData,target)    
    target.close() 
    target = open('./pickle/trainLabel_' + sys.argv[1] + '.p', 'wb')
    pickle.dump(trainLabel,target)    
    target.close() 

    testData = []
    testLabel = []
    for i in range(0,len(labels)):   # For each data set      
        for filename in os.listdir('./Input/Test/' + labels[i]):    # For each image of this label
            image = io.imread('./Input/Test/' + labels[i] + '/' + filename)
            imgArr = mod.transform(image)
            if len(imgArr) == 0:    # No array returned
                continue
            testData.append(imgArr)  # Append all image vectors to a list   
        testLabel += [labels[i]]*len( os.listdir('./Input/Test/' + labels[i]) )

    print "Test data read completely ", len(testData), len(testLabel)
    target = open('./pickle/testData_' + sys.argv[1] + '.p', 'wb')
    pickle.dump(testData,target)    
    target.close() 
    target = open('./pickle/testLabel_' + sys.argv[1] + '.p', 'wb')
    pickle.dump(testLabel,target)    
    target.close() 

def classify():
    trainData = []
    trainLabel = []
    target = open('./pickle/trainData_' + sys.argv[1] + '.p', 'r')
    trainData = pickle.load(target)
    target.close()
    target = open('./pickle/trainLabel_' + sys.argv[1] + '.p', 'r')
    trainLabel = pickle.load(target)
    target.close()

    testData = []
    testLabel = []
    target = open('./pickle/testData_' + sys.argv[1] + '.p', 'r')
    testData = pickle.load(target)
    target.close()
    target = open('./pickle/testLabel_' + sys.argv[1] + '.p', 'r')
    testLabel = pickle.load(target)
    target.close()

    shuffle_in_unison(trainData, trainLabel)
    shuffle_in_unison(testData, testLabel)
    print "Using multiclass SVM"
    cls.fit(trainData, trainLabel)
    temp = cls.score(testData, testLabel)
    print "Classifying accuracy : " + str(temp) 
    predLabel = cls.predict(testData)
    cm = confusion_matrix(testLabel, predLabel)
    print 'Labels : ', dataDict.keys() 
    print 'Confusion matrix : '
    print cm 
    return cls

def getLabel(imagename, cls):
    image = io.imread(imagename)
    imgArr = mod.transform(image)
    return cls.predict(imgArr)[0]

def markvideo(videofile, boxpicle, cls):
    cap = cv2.VideoCapture(videofile)
    f = open(boxpicle, 'rb')
    p = pickle.load(f)
    while (p):
        ret, im = cap.read()
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        for objs in p[1:]:
            for ob in objs:
                box = ob[1]
                imagename = 'Input/cutouts/'+str(p[0])+'_'+str(ob[0])+'.jpg'
                label = getLabel(imagename, cls)
                cv2.rectangle(im, (box[0], box[1]), (box[2], box[3]), (255,0,0), 3)
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(im, label, (box[0], box[1]), font, 0.75,(0,0,255),2)
        skimage.io.imsave('./Output/markvideo/frame_' + str(p[0]) + '.jpg', im)
        try:
            p = pickle.load(f)
        except:
            p = False

if __name__ == "__main__":
    # Read files if instructed from commandline
    if int(sys.argv[2]):
        readFiles()

    cls = classify()
    markvideo(sys.argv[3], sys.argv[4], cls)
