#CS771 Course Project

##Aim : **Object detection and Classification**

We will break down the project into multiple modules where each module will perform disjoint tasks.
The first module that we begin with is on Backgorund Subtracton(BGS). Each module can have an
input and output folder to keep them disjoint.

###Module 1 : *BGS*
###Module 2 : *Detection*
###Module 3 : *Input data*
###Module 4 : *Classification*

###To do:

+ Get a lot of clean data
+ Training set and test set should be fom different parts of video
+ Use resized images from hog and sift
+ __Shivam__:
    - Blur, histogram, and Classify
    - HOG feaures and classify
    - Verify on Mnist
    - Consider horizontal and vertical orientations for data
+ __Gaurav__:
    - Report
    - Haar
+ __Ishita and Kanu__:
    - Tune parameters