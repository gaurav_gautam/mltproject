# First attempt at drawing bounding boxes around detected objects after background subtraction
# Tunable: 
# Order of applying the morphological operators
# minArea  : Would depend on image dimensions, try to make it independent
# minThresh, maxThresh : When a ractangle is recognized from previous frame
# OverlapArea divided by max or min

# Algorithm:
# Step 1 : Take background subtracted image,
# Step 2 : Fill the voids in objects, remove noise
# Step 3 : Draw contours 
# Step 4 : Draw bounding rectangles

import numpy as np
import cv2
import skimage
import skvideo.io
import skimage.io as io
import pickle

oldObjs = []	# Object list from previous frame
newObjs = []	# Object list from next frame
minArea = 1800
Thresh = 0.85	# Threshold for area overlap


def overlapArea(rect1, rect2):
# Finds overlapping area fraction between 2 rectangles rect1 and rect2
# Source for formula : http://math.stackexchange.com/questions/99565/simplest-way-to-calculate-the-intersect-area-of-two-rectangles
	x_overlap = max(0, min(rect1[2],rect2[2]) - max(rect1[0], rect2[0]))
	y_overlap = max(0, min(rect1[3], rect2[3]) - max(rect1[1],rect2[1]))
	overlap = x_overlap * y_overlap * 1.0
	area1 = (rect1[2] - rect1[0])*(rect1[3] - rect1[1])
	area2 = (rect2[2] - rect2[0])*(rect2[3] - rect2[1])
	return overlap/min(area1, area2)	# Can be tuned


def maxOverlap(rect, oldObjs):
# This function tries to find the maximum overlapping rectangle from oldObjs with rect	
# rect : [x1, y1, x2, y2], where (x1,y1) bottom left, (x2,y2) top right
	maxFrac = -1
	maxOverlapID = -1

	for obj in oldObjs:
		rect2 = obj[1]	# Rectangle corresponding to object
		overlap = overlapArea(rect, rect2)
		if overlap > maxFrac:
			maxOverlapID = obj[0]
			maxFrac = overlap

	return maxFrac, maxOverlapID


def getContours(image, i):
# Takes frame 'i' of video 'image' and get contours,
	# Step 2
	frame = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY).astype(np.uint8)

	# This order can be played with

	# Blurring helps smooth out high frequency noise, maybe due to variation in sensors
	frame = cv2.GaussianBlur(frame, (11, 11), 0)
	skimage.io.imsave('./Output/box1/Blur/' + str(i) + '.jpg', frame)

	kernel = np.ones((5,5),np.uint8)
	# Opening is just another name of erosion followed by dilation. It is useful in removing 
	# scattered pixels because of noise
	frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, kernel, iterations = 3)
	skimage.io.imsave('./Output/box1/Open/' + str(i) + '.jpg', frame)

	# Closing is reverse of Opening, Dilation followed by Erosion. It is useful in closing small
	# holes inside the foreground objects, or small black points on the object.
	frame = cv2.morphologyEx(frame, cv2.MORPH_CLOSE, kernel, iterations = 3)
	skimage.io.imsave('./Output/box1/Close/' + str(i) + '.jpg', frame)

	# Dilate the thresholded image to fill in holes
	frame = cv2.dilate(frame, None, iterations=2)
	skimage.io.imsave('./Output/box1/Dilate/' + str(i) + '.jpg', frame)

	# Threshold the frame for further operation
	frame = cv2.threshold(frame, 5, 255, cv2.THRESH_BINARY)[1]
	# frame = cv2.adaptiveThreshold(frame,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV,11,2)
	skimage.io.imsave('./Output/box1/Thresh/' + str(i) + '.jpg', frame)

	# Step 3
	# Draw all contours, Approx to connect points by straight line and saves memory
	(contours, _) = cv2.findContours(frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

	return contours

objId = 0				# This gives the ID of each detected object
#Step 1:
cap = cv2.VideoCapture('./Input/mog2.avi')	# Get this frame for final drawing
cap2 = cv2.VideoCapture('./Input/orig.avi') # Open colored video
i = 0
_, origFrame = cap2.read()
ret, image = cap.read()
target = open('objList.p', 'wb')	# A list of all object rectangles from each frame
# Format of file : 
# [ frameId, [[objId, [x1,y1,x2,y2]], [objId, [x1,y1,x2,y2]], ..]    ]
# [ frameId, [[objId, [x1,y1,x2,y2]], [objId, [x1,y1,x2,y2]], ..]    ] ..
# So each pickle.load(target) will give next frame information

while(ret):
        origFrame = cv2.cvtColor(origFrame, cv2.COLOR_BGR2RGB)
	contours = getContours(image, i)
	cv2.drawContours(image, contours, -1, (0,255,0), 3)
	skimage.io.imsave('./Output/box1/Cont/' + str(i) + '.jpg', image)

	for cnt in contours:
		if cv2.contourArea(cnt) < minArea:			# If the contour is too small, ignore it
			continue
		x,y,w,h = cv2.boundingRect(cnt)
		# Step 4
		cv2.rectangle(origFrame, (x,y), (x+w,y+h),(255,0,0),3)
		newRect = [x,y,x+w,y+h]
		frac, maxId = maxOverlap(newRect, oldObjs)

		newId = -1
		if frac > Thresh:
			newId = maxId
		else :
			objId = (objId + 1)%100000				# Assuming less than 100000 objects in a frame
			newId = objId
		newObjs.append([newId, newRect])
		font = cv2.FONT_HERSHEY_SIMPLEX	
		cv2.putText(origFrame,str(newId),(x,y), font, 0.75,(0,0,255),2)	# Write object Id on object

	pickle.dump([i,newObjs], target)
	oldObjs = list(newObjs)
	newObjs = []
	skimage.io.imsave('./Output/box1/Rect/' + str(i) + '.jpg', origFrame)

	i += 1
	if i%10 == 0:
		print i
	_, origFrame = cap2.read()
	ret, image = cap.read()

cap2.release()
cap.release()
