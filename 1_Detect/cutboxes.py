import pickle
import cv2
from PIL import Image

f = open('objList.p', 'rb')
p = pickle.load(f)

cap = cv2.VideoCapture('Output/box1.avi')

while (p):
    print "Frame: ", p[0],
    #Get the frame
    ret, im = cap.read()
    for objs in p[1:]:
        for ob in objs:
            box = ob[1]
            print "id:", ob[0]
            print "box:", box
            #Crop the image
            im2 = Image.fromarray(im)
            im2 = im2.crop((box[0], box[1], box[2], box[3]))
            im2 = im2.resize((60, 166))
            im2.save('Output/cutouts/'+str(p[0])+'_'+str(ob[0])+'.jpg')
    try:
        p = pickle.load(f)
    except EOFError:
        p = False
