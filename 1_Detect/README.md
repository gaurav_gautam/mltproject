Module2 : *Detect*
==================

This module deals with trying multiple methods for detecting objects and drawing ounding boxes around
them

Folder structure
----------------

Input : Contains the input videos from the BGS programs, right now I am using the mog2 output
Output : Contains the output videos 
Code : Contains source code

box1
----
# Algorithm:
# Step 1 : Take background subtracted image,
# Step 2 : Fill the voids in objects, remove noise
# Step 3 : Draw contours 
# Step 4 : Draw bounding rectangles

Command : > make box1